#include <iostream>
#include <queue>
#include <chrono>
#include <thread>
#include <functional>
#include "fcfs.h"

using namespace std::literals;


auto wait = [](auto time) -> int {
  std::this_thread::sleep_for(time);
  std::cout << "I waited " << time.count() << "\n";
  return time.count();
};

int main()
{
  auto two = std::bind(wait, 2s);  
  auto three = std::bind(wait, 3s);  
  auto four = std::bind(wait, 4s);  
  fcfs scheduler;
  scheduler.add(two);
  scheduler.add(three);
  scheduler.add(four);
  scheduler(10h);
}
