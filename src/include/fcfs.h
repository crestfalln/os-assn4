#include "functional"
#include "scheduler.h"
#include <chrono>
#include <deque>

class fcfs : public scheduler {
public:
  int add(std::function<int()> const &func) override;
  int remove(int pid) override;
  std::deque<std::function<int()>> ready_queue;
  void operator()(std::chrono::microseconds timeout = std::chrono::hours(10));
private:
  static std::chrono::high_resolution_clock clock;    
};

