#include <functional>
class scheduler {
  public:
    virtual int add(std::function<int()> const &) = 0;
    virtual int remove(int pid) = 0;
};

