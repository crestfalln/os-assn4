#include "scheduler.h"
#include <chrono>
#include <deque>
#include <functional>

using namespace std::chrono;

class round_robin : public scheduler {
public:
  struct proc;
  int add(std::function<int()> const &func) override;
  int remove(int pid) override;
  std::deque<proc> ready_queue;
  void operator()(std::chrono::microseconds timeout = std::chrono::hours(10));
  round_robin(std::chrono::microseconds const &Run_time);

private:
  static std::chrono::high_resolution_clock clock;
  int cur_pid = 0;
  microseconds run_time;
};

struct round_robin::proc {
  std::function<int()> const &func;
  microseconds const &timeout;
  int pid;
  int operator()();
  proc(std::function<int()> const &Func, int Pid, microseconds const& Timeout )
      : func(Func), pid(Pid), timeout(Timeout) {}
};
