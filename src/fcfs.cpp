#include "fcfs.h"
#define DEBUG
#ifdef DEBUG
#include <iostream>
#endif

using namespace std::literals;
using namespace std::chrono;

std::chrono::high_resolution_clock fcfs::clock ;

int fcfs::add(std::function<int()> const& func) {
  ready_queue.push_front(func);
  return ready_queue.size();
}

int fcfs::remove(int pid) {
  ready_queue.erase(ready_queue.begin() + pid - 1);
  return pid - 1;
}


void fcfs::operator()(std::chrono::microseconds time) {
  auto start = clock.now();
  auto main_timeout = start + time;
  while(!ready_queue.empty()){
    if(main_timeout <= clock.now()){
#ifdef DEBUG
    std::cout << clock.now().time_since_epoch().count() <<"  Timeout reached returning control to main\n";
#endif
      return;
    }
#ifdef DEBUG
    std::cout << clock.now().time_since_epoch().count() <<"  Starting execution of process\n";
#endif
    auto exec = ready_queue.front();
    exec();
#ifdef DEBUG
    std::cout << clock.now().time_since_epoch().count() <<"  Finished execution of process\n";
#endif
    ready_queue.pop_front();
  }
}
