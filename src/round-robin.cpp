#include "round-robin.h"
#include <algorithm>

int round_robin::proc::operator()(){

}

int round_robin::add(std::function<int()> const &func) {
  ready_queue.emplace_back(func, cur_pid, run_time);
  return cur_pid++;
}

int round_robin::remove(int pid) {
  ready_queue.erase(
      std::find_if(ready_queue.begin(), ready_queue.end(),
                   [&pid](decltype(ready_queue)::const_iterator iter) {
                     return (iter->pid == pid);
                   }));
  return pid;
};

